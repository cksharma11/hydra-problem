chop []     = []
chop (1:xs) = xs
chop (n:xs) = (replicate (n-1) (n-1) ++ xs)

--------------------------------------------------

repetedlyChop heads = iterate chop heads

--------------------------------------------------

takeWhileAlive (x: xs) = if null x then [] else x: (takeWhileAlive xs)

--------------------------------------------------

repetedlyChopTillHead heads = takeWhileAlive (repetedlyChopTillHead heads)

--------------------------------------------------

repeatedlyChopTillDead heads = takeWhile (not.null) (iterate chop heads)

--------------------------------------------------

main = do
  putStrLn "Chopping the heads"
  print(repetedlyChopTillHead [4])